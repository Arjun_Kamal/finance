**Finanz**

---

## Install Docker

https://docs.docker.com/engine/install/

---

## Clone

clone project from GIT

---

## Build

docker-compose build

---

## RUN/DEPOLY

docker-compose up


### Add SUPER USER ###

* docker-compose run django python3 manage.py createsuperuser

### Migrate ###

* docker-compose run django python3 manage.py migrate

### DRF URL ###

* http://localhost:8000

### Angular URL ###

* http://localhost:8080

### Invoice URL ###

* http://localhost:8080/invoice/:uuid

### Stripe Test Cards ###

| Number			| Brand				|CVC			|Date				|
|-------------------|-------------------|---------------|-------------------|
|4242424242424242	|Visa				|Any 3 digits	|Any future date	|
|4000056655665556	|Visa (debit)		|Any 3 digits	|Any future date	|
|5555555555554444	|Mastercard			|Any 3 digits	|Any future date	|

