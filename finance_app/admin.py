from django.contrib import admin
from finance_app.models.client import Client
from finance_app.models.project import Project
from finance_app.models.invoice import Invoice
# from finance_app.models.payment import Payment

admin.site.register(Client)
admin.site.register(Project)
admin.site.register(Invoice)
# admin.site.register(Payment)