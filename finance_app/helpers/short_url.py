import json
import requests
import bitly_api

class Bitly:

	def __init__(self):
		self.ACCESS_TOKEN = "2767e97eea7384450e389972c8481c699cb6ff96"

	def shorturl(self,id):
		long_url = f"https://finanz-app-gb.herokuapp.com/invoice/{id}" 
		response = self.shorten(uri=long_url)
		details = json.loads(response.content)
		link = details.get("link") 
		return link

	def shorten(self,uri):
		headers = {
			'Authorization': f'Bearer {self.ACCESS_TOKEN}',
			'Content-Type': 'application/json',
		}
		data = {
			"long_url": uri,
		 	"domain": "bit.ly",
		}
		print(data)
		return requests.post(url="https://api-ssl.bitly.com/v4/shorten", 
			json=data,
			headers=headers)