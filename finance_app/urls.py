from django.views.generic import TemplateView
from django.urls import path
from rest_framework import routers
from rest_framework.schemas import get_schema_view
from rest_framework.permissions import AllowAny
from rest_framework.authtoken import views
from finance_app.views.dev import DevHandler
from finance_app.views.project import ProjectViewSet
from finance_app.views.client import ClientViewSet
from finance_app.views.invoice import InvoiceViewSet
# from finance_app.views.payment import PaymentViewSet
from finance_app.views.user import UserCreate, UserCurrent

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'project', ProjectViewSet, basename="project")
router.register(r'client', ClientViewSet, basename="client")
router.register(r'invoice', InvoiceViewSet, basename="invoice")
# router.register(r'payment', PaymentViewSet, basename="payment")

urlpatterns = [
    path('me/', UserCurrent.as_view(), name="me"),
    path('register/', UserCreate.as_view(), name="register"),
    path('auth/token/', views.obtain_auth_token),
    path('documentation/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url':'openapi-schema'}
    ), name='swagger-ui'),
    path('openapi/', get_schema_view(
                title="Finanz",
                description="",
                version="1.0.0",
                public=True,
                permission_classes=(AllowAny,),
            ), name='openapi-schema'),
    
]

urlpatterns += router.urls