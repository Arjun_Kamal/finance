"""Imported django, models and serializer"""
from rest_framework import viewsets
from finance_app.models.client import Client, ClientSerializer
"""Client viewset class"""
class ClientViewSet(viewsets.ModelViewSet):
    """Client Viewset class field"""
    serializer_class = ClientSerializer
    model = Client
    queryset = Client.objects.all()