from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
class DevHandler(APIView):
    """GET method using APIView"""
    # permission_classes = [AllowAny]
    def get(self, request, **kwargs):
        return Response({}, status=status.HTTP_200_OK)