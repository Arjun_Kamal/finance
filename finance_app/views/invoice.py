"""Imported django, models and serializer"""
from rest_framework import viewsets
from finance_app.models.invoice import Invoice, InvoiceSerializer
"""Invoice viewset class"""
class InvoiceViewSet(viewsets.ModelViewSet):
    """Invoice Viewset class field"""
    serializer_class = InvoiceSerializer
    model = Invoice
    queryset = Invoice.objects.all()
    