"""Imported django, models and serializer"""
from rest_framework import viewsets
from finance_app.models.project import Project, ProjectSerializer
"""Project viewset class"""
class ProjectViewSet(viewsets.ModelViewSet):
	"""Project Viewset class field"""
	serializer_class = ProjectSerializer
	model = Project
	queryset = Project.objects.all()