"""Imported django, models and serializer"""
from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from finance_app.models.user import UserSerializer
"""User viewset class"""
class UserCreate(generics.CreateAPIView):
	"""User Viewset class field"""
	queryset = User.objects.all()
	serializer_class = UserSerializer

class UserCurrent(APIView):
	"""Get current user  - GET method"""
	def get(self, request, format=None):
		serializer = UserSerializer(request.user)
		return Response(serializer.data)
		