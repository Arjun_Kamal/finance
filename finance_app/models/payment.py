# """Imported Django modules"""
# from django.db import models
# from rest_framework import serializers
# """Payment model class"""
# class Payment(models.Model):
#     """Payment model fields"""
#     amount = models.IntegerField()
#     currency = models.CharField(max_length=3, default="USD")
#     transaction = models.JSONField(null=True, blank=True)
#     status = models.CharField(max_length=50, default="pending")
#     """Payment model meta class"""
#     class Meta:
#         """Payment models meta class fields"""
#         db_table = 'Payment'
#         app_label = 'finance_app'

# class PaymentSerializer(serializers.ModelSerializer):
#     """Payment serializer fields"""
#     amount = serializers.IntegerField(required=True)
#     currency = serializers.CharField(required=True)
#     transaction = serializers.JSONField(required=False)
#     status = serializers.CharField(required=False)
#     """Payment serializer meta class"""
#     class Meta:
#         """Payment serializer meta class fields"""
#         model = Payment
#         fields = ('id', 'amount', 'currency', 'transaction', 'status')
