"""Imported UUID and Django modules"""
import uuid
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxLengthValidator
from rest_framework import serializers
from finance_app.models.user import UserSerializer
"""Client model class"""
class Client(models.Model):
    """Client model fields"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    company = models.CharField(max_length=250, blank=False)
    created_on = models.DateTimeField(auto_now_add=True)
    country = models.CharField(max_length=2, default="US")
    line1 = models.CharField(max_length=250, blank=True, null=True)
    line2 = models.CharField(max_length=250, blank=True, null=True)
    phone = models.PositiveIntegerField(blank=True, null=True, validators=[MaxLengthValidator(12)])
    postal_code = models.CharField(max_length=150, blank=False)
    city = models.CharField(max_length=150, blank=True, null=True)
    """Client model meta class"""
    class Meta:
        """Client model meta class fields"""
        db_table = 'Client'
        app_label = 'finance_app'

class ClientSerializer(serializers.ModelSerializer):
    """Client serializer meta class"""
    user = UserSerializer(read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(
            queryset=User.objects.all(),
            required=True,
            source='user',
            write_only=True)
    class Meta:
        """Client serializer meta class fields"""
        model = Client
        fields = '__all__'
