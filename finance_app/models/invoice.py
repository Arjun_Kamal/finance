"""Imported UUID, Django modules, model and serializer classes"""
import uuid
from django.db import models
from django.core.validators import RegexValidator
from rest_framework import serializers
from finance_app.models.client import Client, ClientSerializer
from finance_app.models.project import Project, ProjectSerializer
# from finance_app.models.payment import Payment, PaymentSerializer
from finance_app.helpers.short_url import Bitly

ALPHANUMERIC = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')
"""Invoice model class"""
class Invoice(models.Model):
    """Invoice model fields"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    invoice_id = models.CharField(max_length=50, validators=[ALPHANUMERIC], unique=True)
    created_on = models.DateTimeField(auto_now_add=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    link = models.URLField(max_length=250, null=True, blank=True)
    amount = models.IntegerField(null=True, blank=True)
    currency = models.CharField(max_length=3, default="USD")
    transaction = models.JSONField(null=True, blank=True)
    status = models.CharField(max_length=50, default="pending")
    """Invoice model meta class"""
    class Meta:
        """Invoice model meta field"""
        db_table = 'Invoice'
        app_label = 'finance_app'

class InvoiceSerializer(serializers.ModelSerializer):
    """Invoice serializer class fields"""
    client = ClientSerializer(read_only=True)
    project = ProjectSerializer(read_only=True)
    client_id = serializers.PrimaryKeyRelatedField(
        queryset=Client.objects.all(),
        required=True,
        source='client',
        write_only=True)
    project_id = serializers.PrimaryKeyRelatedField(
        queryset=Project.objects.all(),
        required=True,
        source='project',
        write_only=True)

    """Invoice serializer meta class"""
    class Meta:
        """Invoice model meta field"""
        model = Invoice
        fields = '__all__'

    def create(self, validated_data):
        invoice = Invoice.objects.create(**validated_data)
        bitly = Bitly()
        invoice.link = bitly.shorturl(invoice.id)
        invoice.save()
        return invoice
