"""Imported UUID and Django modules"""
import uuid
from django.db import models
from rest_framework import serializers

"""Project model class"""
class Project(models.Model):
    """Project model fields"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=150)
    created_on = models.DateTimeField(auto_now_add=True)
    description = models.TextField(null=True, blank=True)
    
    """Project model meta class"""
    class Meta:
       db_table = 'Project'
       app_label = 'finance_app'


"""Project serializer class"""
class ProjectSerializer(serializers.ModelSerializer):
    # amount = serializers.IntegerField(required=True)
    # currency = serializers.CharField(required=True)
    # transaction = serializers.JSONField(required=False)
    # status = serializers.CharField(required=False)
    """Project serializer meta class"""
    class Meta:
        model = Project
        fields = ('id', 'name', 'created_on', 'description')
